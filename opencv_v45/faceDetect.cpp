#include "faceDetect.h"
#include<fstream>
#include<iostream>
using namespace cv;

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "SharedOcv", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "SharedOcv", __VA_ARGS__))


extern "C" {

    uint8_t* resultBuffer;
    char* buffer;


    FaceDetector::FaceDetector()
    {
        meanVal = cv::Scalar(104.0, 177.0, 123.0);
    }

    FaceDetector:: ~FaceDetector()
    {

    }

    void FaceDetector::detectFace(cv::Mat& imgframe)
    {
        int frameHeight = imgframe.rows;
        int frameWidth = imgframe.cols;
        cv::Mat inputBlob = cv::dnn::blobFromImage(imgframe, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, false, false);
        net.setInput(inputBlob, "data");
        cv::Mat detection = net.forward("detection_out");

        cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

        for (int i = 0; i < detectionMat.rows; i++)
        {
            float confidence = detectionMat.at<float>(i, 2);

            if (confidence > confidenceThreshold)
            {
                int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
                int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
                int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
                int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);

                cv::rectangle(imgframe, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0), 2, 4);
            }
        }

    }
    void FaceDetector::loadModelFile(std::string caffeConfigFile, std::string caffeWeightFile)
    {
        net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
    }

    static FaceDetector facedetector_;

    void startDetector(char* configfile, char* weightfile)
    {
        facedetector_.loadModelFile(configfile, weightfile);
    }

    // to receive unity image buffer and detect face
    uint8_t* facedetector(int width, int height, uint8_t* buffer) {
        Mat inFrame = Mat(height, width, CV_8UC4, buffer);
        Mat outImage = inFrame.clone();
        //std::cerr << "read image" << std::endl;
        cv::rotate(outImage, outImage, cv::ROTATE_90_CLOCKWISE);
        cv::cvtColor(outImage, outImage, COLOR_RGBA2BGR);
        //std::cerr << "detection started" << std::endl;
        facedetector_.detectFace(outImage);
        cv::cvtColor(outImage, outImage, COLOR_BGR2RGBA);
        // std::cerr << "detection ended" << std::endl;
        size_t size = width * height * 4;
        memcpy(resultBuffer, outImage.data, size);
        outImage.release();
        return resultBuffer;
    }

    int initBuffer(int width, int height) {
        size_t size = width * height * 4;
        resultBuffer = new uint8_t[size];
        return 0;
    }

    int deleteBuffer() {

        resultBuffer = NULL;
        return 0;
    }

    // rotating image CW by 90degree, to solve the weird problem of texture rotation because of unity webcamtexture
    uint8_t* rotate90Degree(int width, int height, uint8_t* buffer) {
        Mat inFrame = Mat(height, width, CV_8UC4, buffer);
        cv::rotate(inFrame, inFrame, cv::ROTATE_90_CLOCKWISE);
        size_t size = width * height * 4;
        memcpy(resultBuffer, inFrame.data, size);
        inFrame.release();
        return resultBuffer;
    }





}